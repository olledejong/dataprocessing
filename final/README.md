FINAL ASSIGNMENT [DATAPROCESSING]

description:
A tool that maps and sorts fastq file to a genome.


Required to run:
- input:
a genome + index!
fastq file(s)

- programs:
bwa
samtools
bcftools

How to run:
Edit path to genome folder
Edit path to fastq file(s) folder

cmd run: Snakemake --snakefile [name of snakefile]


